package be.oslo.demofirstspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoFirstSpringApplication {

    public static void main(String[] args) {
        System.out.println("TEST");
        SpringApplication.run(DemoFirstSpringApplication.class, args);
    }

}
